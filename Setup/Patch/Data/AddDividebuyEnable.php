<?php
namespace Dividebuy\RetailerConfig\Setup\Patch\Data;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddDividebuyEnable implements DataPatchInterface
{
   /** @var ModuleDataSetupInterface */
   private $moduleDataSetup;

   /** @var EavSetupFactory */
   private $eavSetupFactory;

   /**
    * @param ModuleDataSetupInterface $moduleDataSetup
    * @param EavSetupFactory $eavSetupFactory
    */
   public function __construct(
       ModuleDataSetupInterface $moduleDataSetup,
       EavSetupFactory $eavSetupFactory
   ) {
       $this->moduleDataSetup = $moduleDataSetup;
       $this->eavSetupFactory = $eavSetupFactory;
   }

   /**
    * {@inheritdoc}
    */
   public function apply()
   {
       /** @var EavSetup $eavSetup */
       $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

       $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY, 'dividebuy_enable', [
           'type' => 'int',
           'group' => 'DivideBuy',
           'backend' => '',
           'frontend' => '',
           'label' => 'DivideBuy Enabled',
           'input' => 'select',
           'class' => '',
           'source' => \Magento\Catalog\Model\Product\Attribute\Source\Boolean::class,
           'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
           'visible' => true,
           'required' => false,
           'user_defined' => true,
           'default' => '1',
           'searchable' => false,
           'filterable' => false,
           'comparable' => false,
           'visible_on_front' => false,
           'unique' => false,
           'apply_to' => '',
       ]);

       $eavSetup->addAttribute(\Magento\Catalog\Model\Product::ENTITY, 'dividebuy_tax_class', [
           'type' => 'int',
           'group' => 'DivideBuy',
           'backend' => '',
           'frontend' => '',
           'label' => 'DivideBuy Tax Class',
           'input' => 'select',
           'class' => '',
           'source' => '',
           'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
           'visible' => true,
           'required' => false,
           'user_defined' => true,
           'searchable' => false,
           'filterable' => false,
           'comparable' => false,
           'visible_on_front' => false,
           'unique' => false,
           'apply_to' => '',
           'option' => [
                'values' => [
                    20 => 'VAT Standard - 20%',
                    7 => 'VAT Reduced - 7%',
                    5 => 'VAT Reduced - 5%',
                    0 => 'VAT Zero - 0%',
                ],
            ],
       ]);
   }

   /**
    * {@inheritdoc}
    */
   public static function getDependencies()
   {
       return [];
   }

   /**
    * {@inheritdoc}
    */
   public function getAliases()
   {
       return [];
   }

   /**
   * {@inheritdoc}
   */
   public static function getVersion()
   {
      return '1.0.3';
   }
}