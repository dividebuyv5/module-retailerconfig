<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Controller\Ajax;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;

class Modal extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  private JsonFactory $resultJsonFactory;

  public function __construct(Context $context, JsonFactory $resultJsonFactory)
  {
    parent::__construct($context);

    $this->resultJsonFactory = $resultJsonFactory;
  }

  public function execute()
  {
    $result = $this->resultJsonFactory->create();

    return $result->setData(['success' => true]);
  }
}
