<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Controller\Index;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use Magento\Directory\Model\Currency;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Template;

class GetInstalments extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected Template $templateBlock;

  private Currency $currency;

  private PriceCurrencyInterface $priceCurrency;

  private ResponseHelper $responseHelper;

  public function __construct(
      Context $context,
      Currency $currency,
      Template $templateBlock,
      PriceCurrencyInterface $priceCurrency,
      ResponseHelper $responseHelper
  ) {
    parent::__construct($context);

    $this->currency = $currency;
    $this->templateBlock = $templateBlock;
    $this->priceCurrency = $priceCurrency;
    $this->responseHelper = $responseHelper;
  }

  /**
   * Used get instalment details and load instalmentsDetails.phtml file.
   *
   * @return ResponseInterface|ResultInterface
   *
   * @throws LocalizedException
   * @throws NoSuchEntityException
   */
  public function execute()
  {
    $priceWithCurrency = (string) $this->getRequest()->getparam('price');
    $currencySymbol = (string) $this->currency->getCurrencySymbol();

    $actualProductPrice = ltrim(str_replace(',', '', $priceWithCurrency), $currencySymbol);
    $instalmentDetails = $this->getInstalmentDetails($actualProductPrice);

    $modalBlock = $this->templateBlock
        ->getLayout()
        ->createBlock(Template::class)
        ->assign(['instalment' => [$instalmentDetails, $priceWithCurrency]])
        ->setTemplate('Dividebuy_Product::dividebuy/product/instalments/instalmentsDetails.phtml')
        ->toHtml();

    return $this->responseHelper->sendTextResponse($modalBlock);
  }

  /**
   * Used to get instalment details from core config data.
   *
   * @param  mixed  $totalPrice
   *
   * @return array
   *
   * @throws NoSuchEntityException
   */
  public function getInstalmentDetails($totalPrice): array
  {
    $instalmentDetails = [];

    if ($totalPrice <= 0) {
      return $instalmentDetails;
    }

    $instalments = $this->responseHelper->getConfigHelper()->getInstallments();
    $currencyCode = $this->responseHelper->getConfigHelper()->getStore()->getCurrentCurrencyCode();

    foreach ($instalments as $i => $instalment) {
      $instalmentPrice = $totalPrice / $instalment['key'];
      $isAvailable = $totalPrice >= $instalment['value'] ? 'yes' : 'no';

      $instalmentDetails[] = [
          'available' => $isAvailable,
          'months' => $instalment['key'],
          'value' => $this->priceCurrency->format(
              $instalmentPrice,
              true,
              PriceCurrencyInterface::DEFAULT_PRECISION,
              null,
              $currencyCode
          ),
      ];
    }

    return $instalmentDetails;
  }
}
