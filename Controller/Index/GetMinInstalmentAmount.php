<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Controller\Index;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\CartHelper;
use Dividebuy\Common\Utility\ResponseHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Dividebuy\CheckoutConfig\Block\Cart as CheckoutBlock;
use Dividebuy\Common\Utility\StoreConfigHelper;

class GetMinInstalmentAmount extends Action implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  private ResponseHelper $responseHelper;

  private CartHelper $cartHelper;

  private StoreConfigHelper $storeConfigHelper;

  /**
     * @var CheckoutBlock
     */
    protected $_checkoutBlock;

  public function __construct(Context $context, ResponseHelper $responseHelper, CartHelper $cartHelper,CheckoutBlock $checkoutBlock,StoreConfigHelper $storeConfigHelper)
  {
    parent::__construct($context);

    $this->responseHelper = $responseHelper;
    $this->cartHelper = $cartHelper;
    $this->_checkoutBlock  = $checkoutBlock;
    $this->storeConfigHelper = $storeConfigHelper;
  }

  /**
   * Used get instalment details and load instalmentsDetails.phtml file.
   *
   * @return ResponseInterface|ResultInterface|void
   */
  public function execute()
  {
    $store = $this->storeConfigHelper->getStoreId();
    $price = $this->getRequest()->getparam('price');
    $tooltipType = $this->getRequest()->getParam('tooltipType');
    $financial_promotions_text = $this->storeConfigHelper->getProductPromotionText();
    $financial_calculation = $this->storeConfigHelper->getFinanceCalculation();
    $apr_rate = '';

    if ($tooltipType === 'product') {
      $this->cartHelper->getItemArray();
      $productTotal = $this->cartHelper->getDivideBuyTotal();
      if($financial_calculation == 1){
        $price += (float) $productTotal;
      }
    }

    $instalments = $this->responseHelper->getConfigHelper()->getInstallments();
    $instalments_ibc = $this->responseHelper->getConfigHelper()->getIbcInstallments();
    $instalments = array_merge($instalments,$instalments_ibc);
    $amount = 0;
    $interest_rate = 0;
    $min_order_amount = $this->storeConfigHelper->getMinOrderAmount($store);
    $max_order_amount = $this->storeConfigHelper->getMaxOrderAmount($store);
    usort($instalments, function ($a, $b) {
      return $a['key'] <=> $b['key'];
    });
    foreach ( $instalments as $instalment ){
      if($price >= $instalment['min'] && $price <= $instalment['max']){
        $instalment_amount = $this->_checkoutBlock->calculateInstalmentAmountsForLoan($price, $instalment['interest_rate'] / 100, $instalment['key']);
        $amount = number_format($instalment_amount['regularMonthlyInstalment'],2); 
        $interest_rate = $instalment['interest_rate'];
        $amount = str_replace(",", "", $amount);
        if ($tooltipType === 'product') {
          if($financial_promotions_text == "finance_options"){
            $tooltip_text = "Finance options available - click here to find out more ";
          }else{
            if($financial_calculation == 1){
              $tooltip_text = "Or from £" .$amount. " p/m, (including your total basket value) at " .$interest_rate. "% Representative APR with ";
            }
            else{
              $tooltip_text = "Or from £" .$amount. " p/m at " .$interest_rate. "% Representative APR with ";
            }
          }
        }else{
          $tooltip_text = "Or from £" .$amount. " p/m, " .$interest_rate. "% Representative APR with ";
        }
      }
      else if($price < $min_order_amount){
        $tooltip_text = "Spend at least £" .$min_order_amount. " to spread the cost with ";
        $apr_rate_raw = $this->_checkoutBlock->GetAprRate();
        $apr_rate = number_format(floatval($apr_rate_raw), 2, '.', ',');
      }
      else if($price > $max_order_amount){
        $tooltip_text = "Spend no more than £" .$max_order_amount. " to spread the cost with ";
        $apr_rate_raw = $this->_checkoutBlock->GetAprRate();
        $apr_rate = number_format(floatval($apr_rate_raw), 2, '.', ',');
      }
    }
      
      $min_instalment = array("tooltip_text"=>$tooltip_text);
      /** @var \Magento\Framework\Controller\Result\Json $response */
      $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
      $response->setData(['tooltip_text'=>$tooltip_text, "apr_rate" => $apr_rate, "instalments_ibc" => $instalments_ibc]);
      return $response;
  }
}
