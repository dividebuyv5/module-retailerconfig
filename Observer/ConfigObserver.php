<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Observer;

use Dividebuy\Common\BackendSession;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Constants\XmlFilePaths;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Dividebuy\Common\EventObserver;
use Exception;
use Magento\Backend\Model\Session;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Eav\Model\Entity\Attribute;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;
use Magento\Framework\Message\ManagerInterface;

class ConfigObserver implements ObserverInterface
{
  /**
   * @var Session | BackendSession
   */
  protected Session $backendModelSession;
  protected File $fileManagement;
  protected Filesystem $fileSystem;
  protected Attribute $eavAttribute;
  protected ManagerInterface $messageManager;
  protected Config $resourceConfig;
  private StoreConfigHelper $configHelper;
  private ResponseHelper $responseHelper;
  private ApiHelper $apiHelper;

  public function __construct(
      Session $backendModelSession,
      File $file,
      Filesystem $fileSystem,
      Attribute $eavAttribute,
      ManagerInterface $messageManager,
      Config $resourceConfig,
      StoreConfigHelper $configHelper,
      ResponseHelper $responseHelper,
      ApiHelper $apiHelper
  ) {
    $this->backendModelSession = $backendModelSession;
    $this->fileManagement = $file;
    $this->fileSystem = $fileSystem;
    $this->eavAttribute = $eavAttribute;
    $this->messageManager = $messageManager;
    $this->resourceConfig = $resourceConfig;
    $this->configHelper = $configHelper;
    $this->responseHelper = $responseHelper;
    $this->apiHelper = $apiHelper;
  }

  /**
   * @param  Observer| EventObserver  $observer
   *
   * @throws LocalizedException
   */
  public function execute(Observer $observer)
  {
    $storeCode = $observer->getStore();
    $storeId = $this->configHelper->getStoreId();
    $mediaDir = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath().DivideBuy::DIVIDEBUY_MEDIA_DIR;

    $this->processRetailerImage($mediaDir, $storeId);
    $this->processProductBannerImage($mediaDir, $storeId);
    $this->processCheckoutButtonHoverImage($mediaDir, $storeId);
    $this->processCheckoutButtonImage($mediaDir, $storeId);
    if($storeCode == ''){
      $globalDeactivate = $this->configHelper->getGlobalDeactivateFlag($storeId);
    }
    else{
      $globalDeactivate = $this->configHelper->getGlobalDeactivateFlag($storeCode);
    }

    if ($globalDeactivate !== 1) {
      $extensionStatus = $this->configHelper->getExtensionStatus($storeCode);
      $this->backendModelSession->getPreviousExtensionStatus($storeCode);
      if ($extensionStatus !== $this->backendModelSession->getPreviousExtensionStatus($storeCode) && $extensionStatus != '') {
        $this->updateRetailerStatusInDividebuy($extensionStatus, $storeCode);
      }

      // unsetting all the sessions
      $this->backendModelSession->unsPreviousRetailerImage();
      $this->backendModelSession->unsPreviousProductBannerImage();
      $this->backendModelSession->unsPreviousCheckoutButtonHoverImage();
      $this->backendModelSession->unsPreviousCheckoutButtonImage();
      $this->backendModelSession->unsPreviousExtensionStatus();

      $this->updateInDividebuy($storeCode);
      $this->setDividebuyEnableDefaultValue($storeId);
    } else {
      $this->messageManager->addError(
          __('Retail Partner has been deactivated from DivideBuy. If you believe this is in error, please contact your account manager or email: retailpartners@dividebuy.co.uk')
      );
      $this->clearRetailerConfigurations($storeId);
    }
  }

  public function processRetailerImage($mediaDir, $storeId)
  {
    $retailerImage = $this->configHelper->getRetailerImage($storeId);
    $previous = $this->backendModelSession->getPreviousRetailerImage();

    $this->deleteIfExistAndNotDefault(
        $previous && $previous !== $retailerImage,
        $mediaDir.$previous
    );
  }

  public function processProductBannerImage($mediaDir, $storeId)
  {
    $productBannerImage = $this->configHelper->getButtonImage($storeId);
    $previous = $this->backendModelSession->getPreviousProductBannerImage();

    $this->deleteIfExistAndNotDefault(
        $previous && $previous !== $productBannerImage,
        $mediaDir.$previous
    );
  }

  public function processCheckoutButtonHoverImage($mediaDir, $storeId)
  {
    $checkoutButtonHoverImage = $this->configHelper->getButtonHoverImage($storeId);
    $previous = $this->backendModelSession->getPreviousCheckoutButtonHoverImage();

    $this->deleteIfExistAndNotDefault(
        $previous && $previous !== $checkoutButtonHoverImage,
        $mediaDir.$previous,
        $previous
    );
  }

  public function processCheckoutButtonImage($mediaDir, $storeId)
  {
    $checkoutButtonImage = $this->configHelper->getButtonImage($storeId);
    $previous = $this->backendModelSession->getPreviousCheckoutButtonImage();

    $this->deleteIfExistAndNotDefault(
        $previous && $previous !== $checkoutButtonImage,
        $mediaDir.$previous,
        $previous
    );
  }

  /**
   * Send an API request to Core API to update retailer status.
   *
   * @param  mixed  $retailerStatus
   * @param  mixed  $storeId
   *
   * @return void
   */
  public function updateRetailerStatusInDividebuy($retailerStatus, $storeCode)
  {
    $tokenNumber = $this->configHelper->getStoreToken($storeCode);
    $authNumber = $this->configHelper->getAuthenticationKey($storeCode);
    $allowedIps = $this->configHelper->getAllowedIps($storeCode);
    $params = [
        'storeToken' => $tokenNumber,
        'storeAuthentication' => $authNumber,
        'retailerStatus' => $retailerStatus,
        'allowedIps' => $allowedIps,
    ];

    $this->apiHelper->getSdkApi()->updateRetailerStatus($params);
  }

  public function deleteIfExistAndNotDefault(bool $shouldDelete, $fullPath, $previous = ''): bool
  {
    if (!$shouldDelete || !$this->fileManagement->isExists($fullPath)) {
      return false;
    }

    if ($previous && str_contains((string) $previous, 'default')) {
      return false;
    }

    return $this->fileManagement->deleteFile($fullPath);
  }

  /**
   * updateRetailerStoreCode in divide buy.
   *
   * @param  mixed  $storeCode
   */
  public function updateInDividebuy($storeCode)
  {
    $storeId = $this->configHelper->getCurrentStoreId();
    $tokenNumber = $this->configHelper->getStoreToken($storeId);
    $authNumber = $this->configHelper->getAuthenticationKey($storeId);

    $params = [
        'storeToken' => $tokenNumber,
        'storeAuthentication' => $authNumber,
        'retailerStoreCode' => $storeCode
    ];

    try {
      $response = $this->apiHelper->getSdkApiCurrentStore()->confirmRetailer($params);
      $scope = $storeId ? 'stores' : 'default';
      $storeIdToUse = $storeId ? $storeId : 0;
      if ($response['status'] === 'ok') {
        $this->resourceConfig->saveConfig(XmlFilePaths::XML_PATH_RETAILER_ID, $response['retailerId'], $scope, $storeIdToUse);
      } else {
        $this->responseHelper->debugResponse($response);
      }
    } catch (Exception $e) {
      $this->apiHelper->getLogger()->error($e->getMessage());
    }
  }

  /**
   * Updates the Divide buy enable attribute default value.
   *
   * @param $storeId
   *
   * @throws LocalizedException
   * @throws Exception
   */
  public function setDividebuyEnableDefaultValue($storeId)
  {
    $defaultValue = $this->configHelper->getProductDivideBuyEnableDefaultConfigValue($storeId);

    $attributeCode = 'dividebuy_enable';

    $enableAttribute = $this->eavAttribute->loadByCode('catalog_product', $attributeCode);

    $enableAttribute->setDefaultValue($defaultValue)->save();
  }

  public function clearRetailerConfigurations($storeId)
  {
    $scope = $storeId ? 'stores' : 'default';
    $storeIdToUse = $storeId ? $storeId : 0;

    $this->resourceConfig->saveConfig(XmlFilePaths::XML_PATH_TOKEN_NUMBER, null, $scope, $storeIdToUse);
    $this->resourceConfig->saveConfig(XmlFilePaths::XML_PATH_AUTH_NUMBER, null, $scope, $storeIdToUse);
    $this->resourceConfig->saveConfig(XmlFilePaths::XML_PATH_RETAILER_ID, null, $scope, $storeIdToUse);
    $this->resourceConfig->saveConfig(XmlFilePaths::XML_PATH_DIVIDEBUY_EXTENSION_STATUS, 0, $scope, $storeIdToUse);
  }
}
