<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\Product\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Eav\Model\Entity\Attribute\Source\SourceInterface;
use Magento\Eav\Model\Entity\Collection\AbstractCollection;
use Magento\Framework\Data\OptionSourceInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Product status functionality model.
 */
class Status extends AbstractSource implements SourceInterface, OptionSourceInterface
{
  /**
   * Product Status values.
   */
  public const STATUS_ENABLED = 1;

  public const STATUS_DISABLED = 0;

  /**
   * Retrieve Visible Status Ids.
   *
   * @return array<int>
   */
  public function getVisibleStatusIds(): array
  {
    return [self::STATUS_ENABLED];
  }

  /**
   * Retrieve Saleable Status Ids
   * Default Product Enable status.
   *
   * @return array<int>
   */
  public function getSaleableStatusIds(): array
  {
    return [self::STATUS_ENABLED];
  }

  /**
   * Retrieve option array with empty value.
   *
   * @return array<string>
   */
  public function getAllOptions(): array
  {
    $result = [];

    foreach (self::getOptionArray() as $index => $value) {
      $result[] = ['value' => $index, 'label' => $value];
    }

    return $result;
  }

  /**
   * Retrieve option array.
   *
   * @return array<string>
   */
  public static function getOptionArray(): array
  {
    return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
  }

  /**
   * Retrieve option text by option value.
   *
   * @param string $value
   *
   * @return string
   */
  public function getOptionText($value): ?string
  {
    $options = self::getOptionArray();

    return $options[$value] ?? null;
  }

  /**
   * Add Value Sort To Collection Select.
   *
   * @param AbstractCollection $collection
   * @param string             $dir        direction
   *
   * @throws LocalizedException
   */
  public function addValueSortToCollection($collection, $dir = 'asc'): AbstractSource
  {
    $attributeCode = $this->getAttribute()->getAttributeCode();
    $attributeId = $this->getAttribute()->getId();
    $attributeTable = $this->getAttribute()->getBackend()->getTable();
    $linkField = $this->getAttribute()->getEntity()->getLinkField();

    if ($this->getAttribute()->isScopeGlobal()) {
      $tableName = $attributeCode.'_t';

      $collection->getSelect()->joinLeft(
          [$tableName => $attributeTable],
          "e.{$linkField}={$tableName}.{$linkField} AND {$tableName}.attribute_id='{$attributeId}' AND {$tableName}.store_id='0'",
          []
      );

      $valueExpr = $tableName.'.value';
    } else {
      $valueTable1 = $attributeCode.'_t1';
      $valueTable2 = $attributeCode.'_t2';

      $collection->getSelect()->joinLeft(
          [$valueTable1 => $attributeTable],
          "e.{$linkField}={$valueTable1}.{$linkField} AND {$valueTable1}.attribute_id='{$attributeId}'  AND {$valueTable1}.store_id='0'",
          []
      )->joinLeft(
          [$valueTable2 => $attributeTable],
          "e.{$linkField}={$valueTable2}.{$linkField} AND {$valueTable2}.attribute_id='{$attributeId}' AND {$valueTable2}.store_id='{$collection->getStoreId()}'",
          []
      );

      $valueExpr = $collection->getConnection()->getCheckSql(
          $valueTable2.'.value_id > 0',
          $valueTable2.'.value',
          $valueTable1.'.value'
      );
    }

    $collection->getSelect()->order($valueExpr.' '.$dir);

    return $this;
  }
}
