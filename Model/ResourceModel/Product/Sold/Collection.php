<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\ResourceModel\Product\Sold;

use Magento\Framework\DB\Select;
use Magento\Reports\Model\ResourceModel\Order\Collection as OrderCollection;
use Magento\Sales\Model\Order;

class Collection extends OrderCollection
{
  /**
   * Set Date range to collection.
   *
   * @param int $fromDate
   * @param int $toDate
   *
   * @return $this
   */
  public function setDateRange($fromDate, $toDate): Collection
  {
    $this->_reset()
        ->addAttributeToSelect('*')
        ->addOrderedQty($fromDate, $toDate)
        ->setOrder('ordered_qty')
        ;

    return $this;
  }

  /**
   * Set order.
   *
   * @param string $field
   * @param string $direction
   *
   * @return $this
   */
  public function setOrder($field, $direction = self::SORT_ORDER_DESC): Collection
  {
    if (in_array($field, ['orders', 'ordered_qty'])) {
      $this->getSelect()->order($field.' '.$direction);
    } else {
      parent::setOrder($field, $direction);
    }

    return $this;
  }

  /**
   * Add ordered qty's.
   *
   * @param string $from
   * @param string $to
   *
   * @return $this
   */
  public function addOrderedQty($from = '', $to = ''): Collection
  {
    $connection = $this->getConnection();
    $orderTableAliasName = $connection->quoteIdentifier('order');

    $orderJoinCondition = [
        $orderTableAliasName.'.entity_id = order_items.order_id',
        $connection->quoteInto("{$orderTableAliasName}.state <> ?", Order::STATE_CANCELED),
    ];

    if ($from !== '' && $to !== '') {
      $fieldName = $orderTableAliasName.'.created_at';
      $orderJoinCondition[] = $this->prepareBetweenSql($fieldName, $from, $to);
    }

    $this->getSelect()->reset()->from(
        ['order_items' => $this->getTable('sales_order_item')],
        ['ordered_qty' => 'SUM(order_items.qty_ordered)', 'order_items_name' => 'order_items.name']
    )->joinInner(
        ['order' => $this->getTable('sales_order')],
        implode(' AND ', $orderJoinCondition),
        []
    )->where(
        'parent_item_id IS NULL'
    )->group(
        'order_items.product_id'
    )->having(
        'SUM(order_items.qty_ordered) > ?',
        0
    );

    return $this;
  }

  /**
   * Set store filter to collection.
   *
   * @param array $storeIds
   *
   * @return $this
   */
  public function setStoreIds($storeIds): Collection
  {
    if ($storeIds) {
      $this->getSelect()->where('order_items.store_id IN (?)', (array) $storeIds);
    }

    return $this;
  }

  /**
   * @since 100.2.0
   */
  public function getSelectCountSql(): Select
  {
    $countSelect = clone parent::getSelectCountSql();

    $countSelect->reset(Select::COLUMNS);
    $countSelect->columns('COUNT(DISTINCT order_items.item_id)');

    return $countSelect;
  }

  /**
   * Prepare between sql.
   *
   * @param string $fieldName Field name with table suffix ('created_at' or 'main_table.created_at')
   *
   * @return string Formatted sql string
   */
  protected function prepareBetweenSql(string $fieldName, string $from, string $to): string
  {
    return sprintf(
        '(%s BETWEEN %s AND %s)',
        $fieldName,
        $this->getConnection()->quote($from),
        $this->getConnection()->quote($to)
    );
  }
}
