<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\ResourceModel\Customer\Orders;

use Magento\Reports\Model\ResourceModel\Order\Collection as OrderCollection;

class Collection extends OrderCollection
{
  /**
   * Set date range.
   *
   * @param string $fromDate
   * @param string $toDate
   *
   * @return $this
   */
  public function setDateRange($fromDate, $toDate)
  {
    $this->_reset()->_joinFields($fromDate, $toDate);

    return $this;
  }

  /**
   * Set store filter to collection.
   *
   * @param array $storeIds
   *
   * @return $this
   */
  public function setStoreIds($storeIds): Collection
  {
    if ($storeIds) {
      $this->addAttributeToFilter('store_id', ['in' => (array) $storeIds]);
      $this->addSumAvgTotals(1)->orderByOrdersCount();
    } else {
      $this->addSumAvgTotals()->orderByOrdersCount();
    }

    return $this;
  }

  /**
   * Join fields.
   *
   * @param string $fromDate
   * @param string $toDate
   *
   * @return $this
   */
  protected function _joinFields($fromDate = '', $toDate = ''): Collection
  {
    $this->joinCustomerName()
        ->groupByCustomer()
        ->addOrdersCount()
        ->addAttributeToFilter(
            'created_at',
            ['from' => $fromDate, 'to' => $toDate, 'datetime' => true]
        )
        ;

    return $this;
  }
}
