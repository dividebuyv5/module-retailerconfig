<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\ResourceModel\Customer;

use Magento\Customer\Model\ResourceModel\Customer\Collection as CustomerCollection;
use Magento\Eav\Model\Config as ModelConfig;
use Magento\Eav\Model\EntityFactory as EavEntityFactory;
use Magento\Eav\Model\ResourceModel\Helper;
use Magento\Framework\App\ResourceConnection;
use Magento\Framework\Data\Collection\Db\FetchStrategyInterface;
use Magento\Framework\Data\Collection\EntityFactory;
use Magento\Framework\DataObject\Copy\Config;
use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\DB\Select;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\ResourceModel\Db\VersionControl\Snapshot;
use Magento\Framework\Validator\UniversalFactory;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\ResourceModel\Quote\Item\CollectionFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Order\Collection as OrderCollection;
use Psr\Log\LoggerInterface;

class Collection extends CustomerCollection
{
  /**
   * Add order statistics flag.
   */
  protected bool $_addOrderStatistics = false;

  /**
   * Add order statistics is filter flag.
   */
  protected bool $_addOrderStatFilter = false;

  protected CartRepositoryInterface $quoteRepository;

  protected CollectionFactory $_quoteItemFactory;

  protected OrderCollection $orderResource;

  /**
   * @param  EntityFactory  $entityFactory
   * @param  LoggerInterface  $logger
   * @param  FetchStrategyInterface  $fetchStrategy
   * @param  ManagerInterface  $eventManager
   * @param  ModelConfig  $eavConfig
   * @param  ResourceConnection  $resource
   * @param  EavEntityFactory  $eavEntityFactory
   * @param  Helper  $resourceHelper
   * @param  UniversalFactory  $universalFactory
   * @param  Snapshot  $entitySnapshot
   * @param  Config  $fieldsetConfig
   * @param  CartRepositoryInterface  $quoteRepository
   * @param  CollectionFactory  $quoteItemFactory
   * @param  OrderCollection  $orderResource
   * @param  mixed  $connection
   * @param  string  $modelName
   *
   * @SuppressWarnings(PHPMD.ExcessiveParameterList)
   */
  public function __construct(
      EntityFactory $entityFactory,
      LoggerInterface $logger,
      FetchStrategyInterface $fetchStrategy,
      ManagerInterface $eventManager,
      ModelConfig $eavConfig,
      ResourceConnection $resource,
      EavEntityFactory $eavEntityFactory,
      Helper $resourceHelper,
      UniversalFactory $universalFactory,
      Snapshot $entitySnapshot,
      Config $fieldsetConfig,
      CartRepositoryInterface $quoteRepository,
      CollectionFactory $quoteItemFactory,
      OrderCollection $orderResource,
      ?AdapterInterface $connection = null,
      $modelName = self::CUSTOMER_MODEL_NAME
  ) {
    parent::__construct(
        $entityFactory,
        $logger,
        $fetchStrategy,
        $eventManager,
        $eavConfig,
        $resource,
        $eavEntityFactory,
        $resourceHelper,
        $universalFactory,
        $entitySnapshot,
        $fieldsetConfig,
        $connection,
        $modelName
    );
    $this->orderResource = $orderResource;
    $this->quoteRepository = $quoteRepository;
    $this->_quoteItemFactory = $quoteItemFactory;
  }

  /**
   * Add cart info to collection.
   *
   * @return $this
   */
  public function addCartInfo(): Collection
  {
    foreach ($this->getItems() as $item) {
      try {
        $quote = $this->quoteRepository->getForCustomer($item->getId());

        $totals = $quote->getTotals();
        $item->setTotal($totals['subtotal']->getValue());
        $quoteItems = $this->_quoteItemFactory->create()->setQuoteFilter($quote->getId());
        $quoteItems->load();
        $item->setItems($quoteItems->count());
      } catch (NoSuchEntityException $e) {
        $item->remove();
      }
    }

    return $this;
  }

  /**
   * Add customer name to results.
   *
   * @return $this
   */
  public function addCustomerName(): Collection
  {
    $this->addNameToSelect();

    return $this;
  }

  /**
   * Add order statistics.
   *
   * @param bool $isFilter
   *
   * @return $this
   */
  public function addOrdersStatistics($isFilter = false): Collection
  {
    $this->_addOrderStatistics = true;
    $this->_addOrderStatFilter = (bool) $isFilter;

    return $this;
  }

  /**
   * Order by customer registration.
   *
   * @param string $dir
   *
   * @return $this
   */
  public function orderByCustomerRegistration($dir = self::SORT_ORDER_DESC): Collection
  {
    $this->addAttributeToSort('entity_id', $dir);

    return $this;
  }

  /**
   * Get select count sql.
   */
  public function getSelectCountSql()
  {
    $countSelect = clone $this->getSelect();
    $countSelect->reset(Select::ORDER);
    $countSelect->reset(Select::LIMIT_COUNT);
    $countSelect->reset(Select::LIMIT_OFFSET);
    $countSelect->reset(Select::COLUMNS);
    $countSelect->reset(Select::GROUP);
    $countSelect->reset(Select::HAVING);
    $countSelect->columns('count(DISTINCT e.entity_id)');

    return $countSelect;
  }

  /**
   * Collection after load operations like adding orders statistics.
   *
   * @throws LocalizedException
   *
   * @return $this
   */
  protected function _afterLoad(): Collection
  {
    $this->_addOrdersStatistics();

    return $this;
  }

  /**
   * Add orders statistics to collection items.
   *
   * @throws LocalizedException
   *
   * @return $this
   */
  protected function _addOrdersStatistics(): Collection
  {
    $customerIds = $this->getColumnValues($this->getResource()->getIdFieldName());

    if ($this->_addOrderStatistics && !empty($customerIds)) {
      $connection = $this->orderResource->getConnection();
      $baseSubtotalRefunded = $connection->getIfNullSql('orders.base_subtotal_refunded');
      $baseSubtotalCanceled = $connection->getIfNullSql('orders.base_subtotal_canceled');
      $baseDiscountCanceled = $connection->getIfNullSql('orders.base_discount_canceled');

      $totalExpr = $this->_addOrderStatFilter ?
        "(orders.base_subtotal-{$baseSubtotalCanceled}-{$baseSubtotalRefunded} - {$baseDiscountCanceled}"
        .' - ABS(orders.base_discount_amount))*orders.base_to_global_rate' :
        "orders.base_subtotal-{$baseSubtotalCanceled}-{$baseSubtotalRefunded} - {$baseDiscountCanceled}"
        .' - ABS(orders.base_discount_amount)';

      $select = $this->orderResource->getConnection()->select();
      $select->from(
          ['orders' => $this->orderResource->getTable('sales_order')],
          [
              'orders_avg_amount' => "AVG({$totalExpr})",
              'orders_sum_amount' => "SUM({$totalExpr})",
              'orders_count' => 'COUNT(orders.entity_id)',
              'customer_id',
          ]
      )->where(
          'orders.state <> ?',
          Order::STATE_CANCELED
      )->where(
          'orders.customer_id IN(?)',
          $customerIds
      )->group(
          'orders.customer_id'
      );

      foreach ($this->orderResource->getConnection()->fetchAll($select) as $ordersInfo) {
        $this->getItemById($ordersInfo['customer_id'])->addData($ordersInfo);
      }
    }

    return $this;
  }
}
