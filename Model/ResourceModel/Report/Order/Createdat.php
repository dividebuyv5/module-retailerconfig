<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\ResourceModel\Report\Order;

use DateTime;
use Exception;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Report\AbstractReport;
use Magento\Store\Model\Store;
use Zend_Db_Expr;

class Createdat extends AbstractReport
{
  /**
   * Aggregate Orders data by order created at.
   *
   * @param array|DateTime|int|string|null $from
   * @param array|DateTime|int|string|null $to
   *
   * @throws Exception
   *
   * @return $this
   */
  public function aggregate($from = null, $to = null): Createdat
  {
    return $this->_aggregateByField('created_at', $from, $to);
  }

  /**
   * Aggregate Orders data by custom field.
   *
   * @param array|DateTime|int|string|null $from
   * @param array|DateTime|int|string|null $to
   *
   * @throws Exception
   *
   * @return $this
   *
   * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
   * @SuppressWarnings(PHPMD.UnusedLocalVariable)
   */
  protected function _aggregateByField(string $aggregationField, $from, $to): Createdat
  {
    $connection = $this->getConnection();

    $connection->beginTransaction();

    try {
      if ($from !== null || $to !== null) {
        $subSelect = $this->_getTableDateRangeSelect(
            $this->getTable('sales_order'),
            $aggregationField,
            $aggregationField,
            $from,
            $to
        );
      } else {
        $subSelect = null;
      }
      $this->_clearTableByDateRange($this->getMainTable(), $from, $to, $subSelect);

      $periodExpr = $connection->getDatePartSql(
          $this->getStoreTZOffsetQuery(
              ['o' => $this->getTable('sales_order')],
              'o.'.$aggregationField,
              $from,
              $to
          )
      );
      // Columns list
      $columns = [
          'period' => $periodExpr,
          'store_id' => 'o.store_id',
          'order_status' => 'o.status',
          'orders_count' => new Zend_Db_Expr('COUNT(o.entity_id)'),
          'total_qty_ordered' => new Zend_Db_Expr('SUM(oi.total_qty_ordered)'),
          'total_qty_invoiced' => new Zend_Db_Expr('SUM(oi.total_qty_invoiced)'),
          'total_income_amount' => new Zend_Db_Expr(
              sprintf(
                  'SUM((%s - %s) * %s)',
                  $connection->getIfNullSql('o.base_grand_total'),
                  $connection->getIfNullSql('o.base_total_canceled'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
          'total_revenue_amount' => new Zend_Db_Expr(
              sprintf(
                  'SUM((%s - %s - %s - (%s - %s - %s)) * %s)',
                  $connection->getIfNullSql('o.base_total_invoiced'),
                  $connection->getIfNullSql('o.base_tax_invoiced'),
                  $connection->getIfNullSql('o.base_shipping_invoiced'),
                  $connection->getIfNullSql('o.base_total_refunded'),
                  $connection->getIfNullSql('o.base_tax_refunded'),
                  $connection->getIfNullSql('o.base_shipping_refunded'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
          'total_profit_amount' => new Zend_Db_Expr(
              sprintf(
                  'SUM((%s - %s - %s - %s - %s) * %s)',
                  $connection->getIfNullSql('o.base_total_paid'),
                  $connection->getIfNullSql('o.base_total_refunded'),
                  $connection->getIfNullSql('o.base_tax_invoiced'),
                  $connection->getIfNullSql('o.base_shipping_invoiced'),
                  $connection->getIfNullSql('o.base_total_invoiced_cost'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
          'total_invoiced_amount' => new Zend_Db_Expr(
              sprintf(
                  'SUM(%s * %s)',
                  $connection->getIfNullSql('o.base_total_invoiced'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
          'total_canceled_amount' => new Zend_Db_Expr(
              sprintf(
                  'SUM(%s * %s)',
                  $connection->getIfNullSql('o.base_total_canceled'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
          'total_paid_amount' => new Zend_Db_Expr(
              sprintf(
                  'SUM(%s * %s)',
                  $connection->getIfNullSql('o.base_total_paid'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
          'total_refunded_amount' => new Zend_Db_Expr(
              sprintf(
                  'SUM(%s * %s)',
                  $connection->getIfNullSql('o.base_total_refunded'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
          'total_tax_amount' => new Zend_Db_Expr(
              sprintf(
                  'SUM((%s - %s) * %s)',
                  $connection->getIfNullSql('o.base_tax_amount'),
                  $connection->getIfNullSql('o.base_tax_canceled'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
          'total_tax_amount_actual' => new Zend_Db_Expr(
              sprintf(
                  'SUM((%s -%s) * %s)',
                  $connection->getIfNullSql('o.base_tax_invoiced'),
                  $connection->getIfNullSql('o.base_tax_refunded'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
          'total_shipping_amount' => new Zend_Db_Expr(
              sprintf(
                  'SUM((%s - %s) * %s)',
                  $connection->getIfNullSql('o.base_shipping_amount'),
                  $connection->getIfNullSql('o.base_shipping_canceled'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
          'total_shipping_amount_actual' => new Zend_Db_Expr(
              sprintf(
                  'SUM((%s - %s) * %s)',
                  $connection->getIfNullSql('o.base_shipping_invoiced'),
                  $connection->getIfNullSql('o.base_shipping_refunded'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
          'total_discount_amount' => new Zend_Db_Expr(
              sprintf(
                  'SUM((ABS(%s) - %s) * %s)',
                  $connection->getIfNullSql('o.base_discount_amount'),
                  $connection->getIfNullSql('o.base_discount_canceled'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
          'total_discount_amount_actual' => new Zend_Db_Expr(
              sprintf(
                  'SUM((%s - %s) * %s)',
                  $connection->getIfNullSql('o.base_discount_invoiced'),
                  $connection->getIfNullSql('o.base_discount_refunded'),
                  $connection->getIfNullSql('o.base_to_global_rate')
              )
          ),
      ];

      $select = $connection->select();
      $selectOrderItem = $connection->select();

      $qtyCanceledExpr = $connection->getIfNullSql('qty_canceled');
      $cols = [
          'order_id' => 'order_id',
          'total_qty_ordered' => new Zend_Db_Expr("SUM(qty_ordered - {$qtyCanceledExpr})"),
          'total_qty_invoiced' => new Zend_Db_Expr('SUM(qty_invoiced)'),
      ];
      $selectOrderItem->from(
          $this->getTable('sales_order_item'),
          $cols
      )->where(
          'parent_item_id IS NULL'
      )->group(
          'order_id'
      );

      $select->from(
          ['o' => $this->getTable('sales_order')],
          $columns
      )->join(
          ['oi' => $selectOrderItem],
          'oi.order_id = o.entity_id',
          []
      )->where(
          'o.state NOT IN (?)',
          [Order::STATE_PENDING_PAYMENT, Order::STATE_NEW]
      );

      if ($subSelect !== null) {
        $select->having($this->_makeConditionFromDateRangeSelect($subSelect, 'period'));
      }

      $select->group([$periodExpr, 'o.store_id', 'o.status']);

      $connection->query($select->insertFromSelect($this->getMainTable(), array_keys($columns)));

      // setup all columns to select SUM() except period, store_id and order_status
      foreach ($columns as $k => $v) {
        $columns[$k] = new Zend_Db_Expr('SUM('.$k.')');
      }
      $columns['period'] = 'period';
      $columns['store_id'] = new Zend_Db_Expr(Store::DEFAULT_STORE_ID);
      $columns['order_status'] = 'order_status';

      $select->reset();
      $select->from($this->getMainTable(), $columns)->where('store_id <> 0');

      if ($subSelect !== null) {
        $select->where($this->_makeConditionFromDateRangeSelect($subSelect, 'period'));
      }

      $select->group(['period', 'order_status']);
      $connection->query($select->insertFromSelect($this->getMainTable(), array_keys($columns)));
      $connection->commit();
    } catch (Exception $e) {
      $connection->rollBack();

      throw $e;
    }

    return $this;
  }

  /**
   * Model initialization.
   */
  protected function _construct()
  {
    $this->_init('sales_order_aggregated_created', 'id');
  }
}
