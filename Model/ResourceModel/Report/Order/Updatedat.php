<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\ResourceModel\Report\Order;

class Updatedat extends Createdat
{
  /**
   * Model initialization.
   */
  protected function _construct()
  {
    $this->_init('sales_order_aggregated_updated', 'id');
  }
}
