<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\ResourceModel\Report;

use DateTime;
use Exception;
use Magento\Catalog\Model\Product\Type;
use Magento\Catalog\Model\ResourceModel\Product;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Magento\Framework\Stdlib\DateTime\DateTime as MagentoDateTime;
use Magento\Framework\Stdlib\DateTime\Timezone\Validator;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Reports\Model\Flag;
use Magento\Reports\Model\FlagFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Helper;
use Magento\Sales\Model\ResourceModel\Report\AbstractReport;
use Magento\Store\Model\Store;
use Psr\Log\LoggerInterface;
use Zend_Db_Expr;

class Bestsellers extends AbstractReport
{
  public const AGGREGATION_DAILY = 'daily';

  public const AGGREGATION_MONTHLY = 'monthly';

  public const AGGREGATION_YEARLY = 'yearly';

  protected Product $_productResource;

  protected Helper $_salesResourceHelper;

  /**
   * Ignored product types list.
   */
  protected array $ignoredProductTypes = [
      Type::TYPE_BUNDLE => Type::TYPE_BUNDLE,
  ];

  /**
   * @param  Context  $context
   * @param  LoggerInterface  $logger
   * @param  TimezoneInterface  $localeDate
   * @param  FlagFactory  $reportsFlagFactory
   * @param  Validator  $timezoneValidator
   * @param  MagentoDateTime  $dateTime
   * @param  Product  $productResource
   * @param  Helper  $salesResourceHelper
   * @param  null  $connectionName
   * @param  array  $ignoredProductTypes
   *
   * @SuppressWarnings(PHPMD.ExcessiveParameterList)
   */
  public function __construct(
      Context $context,
      LoggerInterface $logger,
      TimezoneInterface $localeDate,
      FlagFactory $reportsFlagFactory,
      Validator $timezoneValidator,
      MagentoDateTime $dateTime,
      Product $productResource,
      Helper $salesResourceHelper,
      $connectionName = null,
      array $ignoredProductTypes = []
  ) {
    parent::__construct(
        $context,
        $logger,
        $localeDate,
        $reportsFlagFactory,
        $timezoneValidator,
        $dateTime,
        $connectionName
    );
    $this->_productResource = $productResource;
    $this->_salesResourceHelper = $salesResourceHelper;
    $this->ignoredProductTypes = array_merge($this->ignoredProductTypes, $ignoredProductTypes);
  }

  /**
   * Aggregate Orders data by order created at.
   *
   * @param array|DateTime|int|string|null $from
   * @param array|DateTime|int|string|null $to
   *
   * @throws Exception
   *
   * @return $this
   *
   * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
   */
  public function aggregate($from = null, $to = null): Bestsellers
  {
    $connection = $this->getConnection();
    //$this->getConnection()->beginTransaction();

    try {
      if ($from !== null || $to !== null) {
        $subSelect = $this->_getTableDateRangeSelect(
            $this->getTable('sales_order'),
            'created_at',
            'updated_at',
            $from,
            $to
        );
      } else {
        $subSelect = null;
      }

      $this->_clearTableByDateRange($this->getMainTable(), $from, $to, $subSelect);
      // convert dates to current admin timezone
      $periodExpr = $connection->getDatePartSql(
          $this->getStoreTZOffsetQuery(
              ['source_table' => $this->getTable('sales_order')],
              'source_table.created_at',
              $from,
              $to
          )
      );
      $select = $connection->select();

      $select->group([$periodExpr, 'source_table.store_id', 'order_item.product_id']);

      $columns = [
          'period' => $periodExpr,
          'store_id' => 'source_table.store_id',
          'product_id' => 'order_item.product_id',
          'product_name' => new Zend_Db_Expr('MIN(order_item.name)'),
          'product_price' => new Zend_Db_Expr(
              'MIN(IF(order_item_parent.base_price, order_item_parent.base_price, order_item.base_price)) * MIN(source_table.base_to_global_rate)'
          ),
          'qty_ordered' => new Zend_Db_Expr('SUM(order_item.qty_ordered)'),
      ];

      $select->from(
          ['source_table' => $this->getTable('sales_order')],
          $columns
      )->joinInner(
          ['order_item' => $this->getTable('sales_order_item')],
          'order_item.order_id = source_table.entity_id',
          []
      )->joinLeft(
          ['order_item_parent' => $this->getTable('sales_order_item')],
          'order_item.parent_item_id = order_item_parent.item_id',
          []
      )->where(
          'source_table.state != ?',
          Order::STATE_CANCELED
      )->where(
          'order_item.product_type NOT IN(?)',
          $this->ignoredProductTypes
      );

      if ($subSelect !== null) {
        $select->having($this->_makeConditionFromDateRangeSelect($subSelect, 'period'));
      }

      $select->useStraightJoin();
      // important!
      $insertQuery = $select->insertFromSelect($this->getMainTable(), array_keys($columns));
      $connection->query($insertQuery);

      $columns = [
          'period' => 'period',
          'store_id' => new Zend_Db_Expr(Store::DEFAULT_STORE_ID),
          'product_id' => 'product_id',
          'product_name' => new Zend_Db_Expr('MIN(product_name)'),
          'product_price' => new Zend_Db_Expr('MIN(product_price)'),
          'qty_ordered' => new Zend_Db_Expr('SUM(qty_ordered)'),
      ];

      $select->reset();
      $select->from(
          $this->getMainTable(),
          $columns
      )->where(
          'store_id <> ?',
          Store::DEFAULT_STORE_ID
      );

      if ($subSelect !== null) {
        $select->where($this->_makeConditionFromDateRangeSelect($subSelect, 'period'));
      }

      $select->group(['period', 'product_id']);
      $insertQuery = $select->insertFromSelect($this->getMainTable(), array_keys($columns));
      $connection->query($insertQuery);

      // update rating
      $this->_updateRatingPos(self::AGGREGATION_DAILY);
      $this->_updateRatingPos(self::AGGREGATION_MONTHLY);
      $this->_updateRatingPos(self::AGGREGATION_YEARLY);
      $this->_setFlagData(Flag::REPORT_BESTSELLERS_FLAG_CODE);
    } catch (Exception $e) {
      throw $e;
    }

    return $this;
  }

  /**
   * Update rating position.
   *
   * @throws LocalizedException
   *
   * @return $this
   */
  protected function _updateRatingPos(string $aggregation): Bestsellers
  {
    $aggregationTable = $this->getTable('sales_bestsellers_aggregated_'.$aggregation);

    $aggregationAliases = [
        'daily' => self::AGGREGATION_DAILY,
        'monthly' => self::AGGREGATION_MONTHLY,
        'yearly' => self::AGGREGATION_YEARLY,
    ];
    $this->_salesResourceHelper->getBestsellersReportUpdateRatingPos(
        $aggregation,
        $aggregationAliases,
        $this->getMainTable(),
        $aggregationTable
    );

    return $this;
  }

  /**
   * Model initialization.
   */
  protected function _construct()
  {
    $this->_init('sales_bestsellers_aggregated_'.self::AGGREGATION_DAILY, 'id');
  }
}
