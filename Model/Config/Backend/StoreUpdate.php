<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\Config\Backend;

use Dividebuy\Common\BackendSession;
use Dividebuy\Common\Registry;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Magento\Backend\Model\Session;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\ScopeInterface;

class StoreUpdate extends Value
{
    private Session $backendModelSession;
    private StoreConfigHelper $configHelper;
    private StoreManagerInterface $storeManager;
    private ScopeConfigInterface $scopeConfig;

    public function __construct(
        Context $context,
        Session $backendModelSession,
        Registry $registry,
        TypeListInterface $cacheTypeList,
        StoreConfigHelper $configHelper,
        StoreManagerInterface $storeManager,
        ScopeConfigInterface $scopeConfig,
        ?AbstractResource $resource = null,
        ?AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $configHelper->getScopeConfig(),
            $cacheTypeList,
            $resource,
            $resourceCollection,
            $data
        );

        $this->backendModelSession = $backendModelSession;
        $this->configHelper = $configHelper;
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @throws NoSuchEntityException
     *
     * @return StoreUpdate|void
     */
    public function beforeSave()
    {
        // Retrieve the current scope and scope ID
        $scope = $this->getScope();
        $scopeId = $this->getScopeId(); // This will give store_id or website_id based on the scope

        // Determine the store code based on the scope
        if ($scope === ScopeInterface::SCOPE_STORES) {
            // Get store code if scope is stores
            $storeCode = $this->storeManager->getStore($scopeId)->getCode();
        } elseif ($scope === ScopeInterface::SCOPE_WEBSITES) {
            // Get default store of the website if the scope is website
            $website = $this->storeManager->getWebsite($scopeId);
            $storeCode = $website->getDefaultStore()->getCode();
        } else {
            // Default global scope
            $storeCode = 'default'; // You can handle this case as required
        }

        // Example usage of store code in the configuration saving logic
        
        $checkoutButtonImage = $this->configHelper->getButtonImage($storeCode);
        $checkoutButtonHoverImage = $this->configHelper->getButtonHoverImage($scopeId);
        $retailerImage = $this->configHelper->getRetailerImage($scopeId);
        $productBannerImage = $this->configHelper->getProductBanner($storeCode);
        $extensionStatus = $this->configHelper->getExtensionStatus($storeCode);

        $this->backendModelSession->setPreviousRetailerImage($retailerImage);
        $this->backendModelSession->setPreviousProductBannerImage($productBannerImage);
        $this->backendModelSession->setPreviousCheckoutButtonHoverImage($checkoutButtonHoverImage);
        $this->backendModelSession->setPreviousCheckoutButtonImage($checkoutButtonImage);
        $this->backendModelSession->setPreviousExtensionStatus($extensionStatus);

        parent::beforeSave();
    }
}
