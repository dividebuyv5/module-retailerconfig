<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\Config\Source\General;

use Magento\Framework\Option\ArrayInterface;

class RetailerStatus implements ArrayInterface
{
  /**
   * Options getter.
   */
  public function toOptionArray(): array
  {
    return [['value' => 1, 'label' => __('Activate')], ['value' => 0, 'label' => __('Deactivate')]];
  }

  /**
   * Get options in "key-value" format.
   */
  public function toArray(): array
  {
    return [1 => __('Deactivate'), 0 => __('Activate')];
  }
}
