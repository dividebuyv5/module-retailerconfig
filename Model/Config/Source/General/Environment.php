<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\Config\Source\General;

use Magento\Framework\Option\ArrayInterface;

class Environment implements ArrayInterface
{
  public function toOptionArray(): array
  {
    return [
        ['value' => 'development', 'label' => __('Development')],
        ['value' => 'staging', 'label' => __('Staging')],
        ['value' => 'production', 'label' => __('Production')],
    ];
  }
}
