<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\Config\Source\Product;

use Magento\Framework\Option\ArrayInterface;

class FinanceCalculation implements ArrayInterface
{
  public function toOptionArray(): array
  {
    return [
        ['value' => '1', 'label' => __('Yes')],
        ['value' => '0', 'label' => __('No')],
    ];
  }
}
