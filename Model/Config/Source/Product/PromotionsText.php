<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\Config\Source\Product;

use Magento\Framework\Option\ArrayInterface;

class PromotionsText implements ArrayInterface
{
  public function toOptionArray(): array
  {
    return [
        ['value' => 'or_from', 'label' => __('Or from £xx.xx…. ')],
        ['value' => 'finance_options', 'label' => __('Finance options available')],
    ];
  }
}
