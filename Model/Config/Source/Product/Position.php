<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Model\Config\Source\Product;

use Magento\Framework\Option\ArrayInterface;

class Position implements ArrayInterface
{
  public function toOptionArray(): array
  {
    return [
        ['value' => 'before', 'label' => __('Before proceed to checkout button')],
        ['value' => 'after', 'label' => __('After proceed to checkout button')],
    ];
  }
}
