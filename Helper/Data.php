<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Helper;

use Dividebuy\Common\Constants\XmlFilePaths;
use Dividebuy\Common\Registry;
use Dividebuy\Common\Utility\ProductHelper;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Magento\Catalog\Model\Product;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;

class Data extends AbstractHelper
{
  /**
   * Currently selected store ID if applicable.
   *
   * @var mixed
   */
  protected $_storeId;

  protected Registry $coreRegistry;

  private StoreConfigHelper $configHelper;

  private ProductHelper $productHelper;

  public function __construct(
      Context $context,
      Registry $coreRegistry,
      StoreConfigHelper $configHelper,
      ProductHelper $productHelper
  ) {
    $this->coreRegistry = $coreRegistry;
    $this->configHelper = $configHelper;
    $this->productHelper = $productHelper;

    parent::__construct($context);
  }

  /**
   * Retrieve current Product object.
   */
  public function getProduct(): ?Product
  {
    return $this->coreRegistry->registry('current_product');
  }

  /**
   * Retrieve retailer id.
   *
   * @param null $storeId
   *
   * @return mixed
   */
  public function getRetailerId($storeId = null)
  {
    return $this->configHelper->getRetailerId($storeId);
  }

  /**
   * Retrieve store name.
   *
   * @param  null  $storeId
   *
   * @return string
   */
  public function getStoreName($storeId = null): string
  {
    return $this->configHelper->getStoreName($storeId);
  }

  /**
   * Retrieve auth number.
   *
   * @param null $storeId
   *
   * @return mixed
   */
  public function getAuthNumber($storeId = null)
  {
    return $this->configHelper->getAuthenticationKey($storeId);
  }

  /**
   * This function is used for getting list of allowed IPs stored in DivideBuy configuration.
   *
   * @param int $storeId
   *
   * @return mixed
   */
  public function getAllowedIps($storeId = null)
  {
    return $this->configHelper->getAllowedIps($storeId);
  }

  /**
   * Retrieve token number.
   *
   * @param null $storeId
   *
   * @return mixed
   */
  public function getTokenNumber($storeId = null)
  {
    return $this->configHelper->getStoreToken($storeId);
  }

  /**
   * Retrieve product banner css.
   *
   * @param null $storeId
   *
   * @return mixed
   */
  public function getProductBannerCss($storeId = null)
  {
    return $this->configHelper->getStoreScopedValue(XmlFilePaths::XML_PATH_PRODUCT_CUSTOM_CSS, $storeId);
  }

  /**
   * Retrieve product status.
   *
   * @param  null  $storeId
   *
   * @return bool
   */
  public function getProductStatus($storeId = null): bool
  {
    return $this->configHelper->getProductStatus($storeId);
  }

  /**
   * Used to check whether current ip is allowed to see Dividebuy functionality or not.
   *
   * @param  mixed  $storeId
   *
   * @return bool
   */
  public function isIPAllowed($storeId = null): bool
  {
    return $this->configHelper->isIpAllowed($storeId);
  }

  /**
   * Function used for checking if softsearch is enabled on product page.
   *
   * @param null $storeId
   *
   * @return mixed
   */
  public function isProductSoftSearchEnabled($storeId = null)
  {
    return $this->configHelper->getStoreScopedValue(XmlFilePaths::XML_PATH_PRODUCT_SOFT_SEARCH, $storeId);
  }

  /**
   * Function used for checking if softsearch is enabled on cart page.
   *
   * @param null $storeId
   *
   * @return mixed
   */
  public function isCartSoftSearchEnabled($storeId = null)
  {
    return $this->configHelper->isCartSoftSearchEnabled($storeId);
  }

  /**
   * Function used for checking if softsearch is enabled on checkout page.
   *
   * @param null $storeId
   *
   * @return mixed
   */
  public function isCheckoutSoftSearchEnabled($storeId = null)
  {
    return $this->configHelper->isCheckoutSoftSearchEnabled($storeId);
  }

  /**
   * Retrieve retailer logo url.
   *
   * @throws NoSuchEntityException
   */
  public function getRetailerLogoUrl(): string
  {
    return $this->configHelper->getRetailerLogoUrl();
  }

  /**
   * Get base URL.
   *
   * @param string $scope
   *
   * @throws NoSuchEntityException
   */
  public function getBaseUrl($scope = null): string
  {
    return (string) $this->configHelper->getStore()->getBaseUrl($scope);
  }

  /**
   * Used to retrive storeID.
   *
   * @throws NoSuchEntityException
   *
   * @return mixed
   */
  public function getStoreId()
  {
    return $this->configHelper->getStoreId();
  }

  /**
   * Retrieve product banner url.
   *
   * @throws NoSuchEntityException
   */
  public function getProductBannerUrl(): string
  {
    return $this->configHelper->getProductBannerUrl();
  }

  /**
   * Retrieve Checkout banner url.
   *
   * @throws NoSuchEntityException
   */
  public function getCheckoutBannerUrl(): string
  {
    return $this->configHelper->getCheckoutBannerUrl();
  }

  /**
   * Used to get instalment details from core config data.
   *
   * @param mixed $totalPrice
   *
   * @throws NoSuchEntityException
   */
  public function getInstalmentDetails($totalPrice): array
  {
    $i = 0;
    $instalmentDetails = [];
    $instalments = $this->configHelper->getInstallments();
    $currencyCode = $this->configHelper->getStore()->getCurrentCurrencyCode();

    if ($totalPrice > 0) {
      foreach ($instalments as $instalment) {
        $instalmentPrice = $totalPrice / $instalment['key'];
        if ($totalPrice >= $instalment['value']) {
          $instalmentDetails[$i]['months'] = $instalment['key'];
          $instalmentDetails[$i]['value'] = $this->productHelper->formatPrice(
              $instalmentPrice,
              $includeContainer = true,
              2,
              $scope = null,
              $currencyCode
          );
          $instalmentDetails[$i]['available'] = 'yes';
        } else {
          $instalmentDetails[$i]['months'] = $instalment['key'];
          $instalmentDetails[$i]['value'] = $this->productHelper->formatPrice(
              $instalmentPrice,
              $includeContainer = true,
              2,
              $scope = null,
              $currencyCode
          );
          $instalmentDetails[$i]['available'] = 'no';
        }
        ++$i;
      }
    }

    return (array) $instalmentDetails;
  }

  /**
   * Used to get instalment details from core config data.
   */
  public function getInstalmentData(): array
  {
    return (array) $this->configHelper->getInstallments();
  }

  /**
   * Function for getting maximum month instalment.
   *
   * @return mixed
   */
  public function getMaximumAvailableInstalments()
  {
    return $this->configHelper->getMaximumAvailableInstalments();
  }

  /**
   * Function used for getting background color.
   */
  public function getBannerBackgroundColor(): string
  {
    return $this->configHelper->getBannerBackgroundColor();
  }

  public function isGuestCheckoutEnabled($storeId = null)
  {
    return $this->configHelper->isGuestCheckoutEnabled($storeId);
  }

  /**
   * Used to get google analytics unique key from configuration.
   *
   * @param mixed $storeId
   */
  public function getGoogleAnalyticUniqueKey($storeId = null): string
  {
    return $this->configHelper->getGoogleAnalyticUniqueKey($storeId);
  }

  /**
   * Returns the configuration value of Set new products to shop with DivideBuy of Dividebuy configuration.
   *
   * @param int $storeId
   *
   * @return mixed
   */
  public function getProductDividebuyEnableDefaultConfigValue($storeId = null)
  {
    return $this->configHelper->getProductDivideBuyEnableDefaultConfigValue($storeId);
  }

  /**
   * Get the value of Activate/Deactivate Dividebuy field value from configuration.
   *
   * @param int $storeId
   *
   * @return mixed
   */
  public function getExtensionStatus($storeId = null)
  {
    return $this->configHelper->getExtensionStatus($storeId);
  }

  /**
   * Function for getting maximum month instalment value.
   *
   * @return mixed
   */
  public function getMaximumAvailableInstalmentValue()
  {
    return $this->configHelper->getMaximumAvailableInstalmentValue();
  }

  /**
   * Used to get the retailer store Token and authentication.
   */
  public function getRetailerStoreTokenAndAuthentication(): array
  {
    return $this->configHelper->getRetailerStoreTokenAndAuthentication();
  }

  /**
   * @return mixed
   */
  public function getSymbol()
  {
    return $this->productHelper->getSymbol();
  }
}
