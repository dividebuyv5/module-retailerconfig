<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Helper\Adminhtml\Dashboard;

use Magento\Backend\Helper\Dashboard\AbstractDashboard;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Reports\Model\ResourceModel\Order\Collection;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;

class Order extends AbstractDashboard
{
  protected Collection $orderCollection;

  protected StoreManagerInterface $storeManager;

  public function __construct(Context $context, Collection $orderCollection, StoreManagerInterface $storeManager)
  {
    $this->orderCollection = $orderCollection;
    $this->storeManager = $storeManager;

    parent::__construct($context);
  }

  /**
   * @throws LocalizedException
   * @throws NoSuchEntityException
   */
  protected function _initCollection()
  {
    $isFilter = $this->getParam('store') || $this->getParam('website') || $this->getParam('group');

    $this->_collection = $this->orderCollection->prepareSummary($this->getParam('period'), 0, 0, $isFilter);

    if ($this->getParam('store')) {
      $this->_collection->addFieldToFilter('store_id', $this->getParam('store'));
    } elseif ($this->getParam('website')) {
      $storeIds = $this->storeManager->getWebsite($this->getParam('website'))->getStoreIds();
      $this->_collection->addFieldToFilter('store_id', ['in' => implode(',', $storeIds)]);
    } elseif ($this->getParam('group')) {
      $storeIds = $this->storeManager->getGroup($this->getParam('group'))->getStoreIds();
      $this->_collection->addFieldToFilter('store_id', ['in' => implode(',', $storeIds)]);
    } elseif (!$this->_collection->isLive()) {
      $this->_collection->addFieldToFilter(
          'store_id',
          ['eq' => $this->storeManager->getStore(Store::ADMIN_CODE)->getId()]
      );
    }

    $this->_collection->load();
  }
}
