<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Block\Adminhtml;

use Magento\Framework\View\Element\Template;

class Notifications extends Template
{
  public function getMessage(): string
  {
    return 'To "Activate/Deactivate" Dividebuy plugin goto Stores>Configuration>Dividebuy>Configuration. Select "Store View", goto General configuration then set "Activate/Deactivte DivideBuy" field  to "Activate/Deactivate".';
  }
}
