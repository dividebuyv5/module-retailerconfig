<?php

namespace Dividebuy\RetailerConfig\Block\Adminhtml\System;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\AbstractBlock;
use Magento\Config\Model\Config\CommentInterface;

class AllowedIp extends AbstractBlock implements CommentInterface
{
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * Constructor
     *
     * @param RequestInterface $request
     */
    public function __construct(
        RequestInterface $request
    ) {
        $this->request = $request;
    }
    public function getCommentText($elementValue)
    {
        $remoteIp = $this->request->getServer('REMOTE_ADDR');
        return __('To limit which users are able to see the DivideBuy functionality, enter a comma-separated list of allowed IP addresses here (Your current IP: ' . $remoteIp . ')');
	}
}