<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Block\Dashboard;

use Magento\Backend\Block\Dashboard\Bar;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Module\Manager;
use Magento\Reports\Model\ResourceModel\Order\CollectionFactory;
use Magento\Store\Model\Store;

class Totals extends Bar
{
  /**
   * @var string
   */
  protected $_template = 'dashboard/totalbar.phtml';

  protected Manager $_moduleManager;

  public function __construct(
      Context $context,
      CollectionFactory $collectionFactory,
      Manager $moduleManager,
      array $data = []
  ) {
    $this->_moduleManager = $moduleManager;
    parent::__construct($context, $collectionFactory, $data);
  }

  /**
   * @throws LocalizedException
   * @throws NoSuchEntityException
   *
   * @return $this
   */
  protected function _prepareLayout(): Totals
  {
    if (!$this->_moduleManager->isEnabled('Magento_Reports')) {
      return $this;
    }

    $isFilter = $this->getRequest()->getParam(
        'store'
    ) || $this->getRequest()->getParam(
        'website'
    ) || $this->getRequest()->getParam(
        'group'
    );
    $period = $this->getRequest()->getParam('period', '24h');

    $collection = $this->_collectionFactory->create()->addCreateAtPeriodFilter(
        $period
    )->calculateTotals(
        $isFilter
    );

    if ($this->getRequest()->getParam('store')) {
      $collection->addFieldToFilter('store_id', $this->getRequest()->getParam('store'));
    } else {
      if ($this->getRequest()->getParam('website')) {
        $storeIds = $this->_storeManager->getWebsite($this->getRequest()->getParam('website'))->getStoreIds();
        $collection->addFieldToFilter('store_id', ['in' => $storeIds]);
      } else {
        if ($this->getRequest()->getParam('group')) {
          $storeIds = $this->_storeManager->getGroup($this->getRequest()->getParam('group'))->getStoreIds();
          $collection->addFieldToFilter('store_id', ['in' => $storeIds]);
        } elseif (!$collection->isLive()) {
          $collection->addFieldToFilter(
              'store_id',
              ['eq' => $this->_storeManager->getStore(Store::ADMIN_CODE)->getId()]
          );
        }
      }
    }

    $collection->load();

    $totals = $collection->getFirstItem();

    $this->addTotal(__('Revenue'), $totals->getRevenue());
    $this->addTotal(__('Tax'), $totals->getTax());
    $this->addTotal(__('Shipping'), $totals->getShipping());
    $this->addTotal(__('Quantity'), $totals->getQuantity() * 1, true);

    return $this;
  }
}
