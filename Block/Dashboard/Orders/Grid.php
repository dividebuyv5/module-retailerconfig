<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Block\Dashboard\Orders;

use Magento\Backend\Block\Dashboard\Grid as DashBoardGrid;
use Magento\Backend\Block\Dashboard\Orders\Grid as MagentoGrid;
use Magento\Framework\Exception\LocalizedException;

class Grid extends MagentoGrid
{
  /**
   * @throws LocalizedException
   *
   * @return $this|DashBoardGrid|Grid
   */
  protected function _prepareCollection()
  {
    if (!$this->_moduleManager->isEnabled('Magento_Reports')) {
      return $this;
    }
    $collection = $this->_collectionFactory->create()->addItemCountExpr()->joinCustomerName(
        'customer'
    )->orderByCreatedAt();

    if ($this->getParam('store') || $this->getParam('website') || $this->getParam('group')) {
      if ($this->getParam('store')) {
        $collection->addAttributeToFilter('store_id', $this->getParam('store'));
      } elseif ($this->getParam('website')) {
        $storeIds = $this->_storeManager->getWebsite($this->getParam('website'))->getStoreIds();
        $collection->addAttributeToFilter('store_id', ['in' => $storeIds]);
      } elseif ($this->getParam('group')) {
        $storeIds = $this->_storeManager->getGroup($this->getParam('group'))->getStoreIds();
        $collection->addAttributeToFilter('store_id', ['in' => $storeIds]);
      }

      $collection->addRevenueToSelect();
    } else {
      $collection->addRevenueToSelect(true);
    }

    $this->setCollection($collection);

    return DashBoardGrid::_prepareCollection();
  }
}
