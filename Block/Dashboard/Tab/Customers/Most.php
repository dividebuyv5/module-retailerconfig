<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Block\Dashboard\Tab\Customers;

use Exception;
use Magento\Backend\Block\Dashboard\Grid as DashboardGrid;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Data;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Reports\Model\ResourceModel\Order\CollectionFactory;

class Most extends DashboardGrid
{
  protected CollectionFactory $_collectionFactory;

  public function __construct(
      Context $context,
      Data $backendHelper,
      CollectionFactory $collectionFactory,
      array $data = []
  ) {
    $this->_collectionFactory = $collectionFactory;
    parent::__construct($context, $backendHelper, $data);
  }

  /**
   * {@inheritdoc}
   */
  public function getRowUrl($item): string
  {
    return $this->getUrl('customer/index/edit', ['id' => $item->getCustomerId()]);
  }

  protected function _construct()
  {
    parent::_construct();
    $this->setId('customersMostGrid');
  }

  /**
   * {@inheritdoc}
   *
   * @throws LocalizedException
   */
  protected function _prepareCollection()
  {
    $collection = $this->_collectionFactory->create();

    $collection->groupByCustomer()->addOrdersCount()->joinCustomerName();

    $storeFilter = 0;
    if ($this->getParam('store')) {
      $collection->addAttributeToFilter('store_id', $this->getParam('store'));
      $storeFilter = 1;
    } elseif ($this->getParam('website')) {
      $storeIds = $this->_storeManager->getWebsite($this->getParam('website'))->getStoreIds();
      $collection->addAttributeToFilter('store_id', ['in' => $storeIds]);
    } elseif ($this->getParam('group')) {
      $storeIds = $this->_storeManager->getGroup($this->getParam('group'))->getStoreIds();
      $collection->addAttributeToFilter('store_id', ['in' => $storeIds]);
    }


    $collection->addSumAvgTotals($storeFilter)->orderByTotalAmount();

    $this->setCollection($collection);

    return parent::_prepareCollection();
  }

  /**
   * {@inheritdoc}
   *
   * @throws NoSuchEntityException
   * @throws Exception
   */
  protected function _prepareColumns()
  {
    $this->addColumn('name', ['header' => __('Customer'), 'sortable' => false, 'index' => 'name']);

    $this->addColumn(
        'orders_count',
        [
            'header' => __('Orders'),
            'sortable' => false,
            'index' => 'orders_count',
            'type' => 'number',
            'header_css_class' => 'col-orders',
            'column_css_class' => 'col-orders',
        ]
    );

    $baseCurrencyCode = (string) $this->_storeManager->getStore(
        (int) $this->getParam('store')
    )->getBaseCurrencyCode();

    $this->addColumn(
        'orders_avg_amount',
        [
            'header' => __('Average'),
            'sortable' => false,
            'type' => 'currency',
            'currency_code' => $baseCurrencyCode,
            'index' => 'orders_avg_amount',
            'header_css_class' => 'col-avg',
            'column_css_class' => 'col-avg',
        ]
    );

    $this->addColumn(
        'orders_sum_amount',
        [
            'header' => __('Total'),
            'sortable' => false,
            'type' => 'currency',
            'currency_code' => $baseCurrencyCode,
            'index' => 'orders_sum_amount',
            'header_css_class' => 'col-total',
            'column_css_class' => 'col-total',
        ]
    );

    $this->setFilterVisibility(false);
    $this->setPagerVisibility(false);

    return parent::_prepareColumns();
  }
}
