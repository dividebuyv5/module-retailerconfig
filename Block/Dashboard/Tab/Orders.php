<?php

declare(strict_types=1);

namespace Dividebuy\RetailerConfig\Block\Dashboard\Tab;

use Dividebuy\RetailerConfig\Helper\Adminhtml\Dashboard\Order;
use Magento\Backend\Block\Dashboard\Graph;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Helper\Dashboard\Data;
use Magento\Reports\Model\ResourceModel\Order\CollectionFactory;

class Orders extends Graph
{
  public function __construct(
      Context $context,
      CollectionFactory $collectionFactory,
      Data $dashboardData,
      Order $dataHelper,
      array $data = []
  ) {
    $this->_dataHelper = $dataHelper;
    parent::__construct($context, $collectionFactory, $dashboardData, $data);
  }

  /**
   * Initialize object.
   */
  protected function _construct()
  {
    $this->setHtmlId('orders');
    parent::_construct();
  }

  /**
   * Prepare chart data.
   */
  protected function _prepareData()
  {
    $this->getDataHelper()->setParam('store', $this->getRequest()->getParam('store'));
    $this->getDataHelper()->setParam('website', $this->getRequest()->getParam('website'));
    $this->getDataHelper()->setParam('group', $this->getRequest()->getParam('group'));

    $this->setDataRows('quantity');
    $this->_axisMaps = ['x' => 'range', 'y' => 'quantity'];

    parent::_prepareData();
  }
}
